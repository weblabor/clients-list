<?php
/**
 * The Movies Custom Type Manager is the core plugin responsible for including and
 * instantiating all of the code that composes the plugin
 *
 * @package    WCLM
 */

class Clients_List_Manager {

    protected $loader;

    public function __construct() {
        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->load_metaboxes();
    }
   
    private function load_dependencies() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'modules/loader.php'; // Load modules classes
        require_once plugin_dir_path( __FILE__ ) . 'class-clients-list-manager-loader.php';
        $this->loader = new Clients_List_Manager_Loader();
    }
    
    private function define_admin_hooks() {
        $customType = new WCL_Custom_Type();
        $this->loader->add_action( 'init', $customType, 'create_post_type' );
    }

    private function load_metaboxes() {
        $telephone = new Metabox_Telephone($this->loader);
        $email = new Metabox_Email($this->loader);
    }
    
    public function run() {
        $this->loader->run();
    }

}