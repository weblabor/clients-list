<?php
/*
  Plugin Name: Clients List
  Description: Show a list of clients
  Author: Carlos Escobar
  Author URI: http://www.weblabor.mx
  Version: 0

  Licenced under the GNU GPL:

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// If this file is called directly, then abort execution.
if ( ! defined( 'WPINC' ) ) {
    die;
}

require_once plugin_dir_path( __FILE__ ) . 'includes/class-clients-list-manager.php';


function run_clients_list_manager() {
    $mct = new Clients_List_Manager();
    $mct->run();
}

run_clients_list_manager();

// Add the custom columns to the client post type:
add_filter( 'manage_client_posts_columns', 'set_custom_edit_client_columns' );
function set_custom_edit_client_columns($columns) {
    $new = array();
    $new['cb'] = '<input type="checkbox" />';
    $new['title'] = __( 'Title', 'your_text_domain' );
    $new['email'] = __( 'Email', 'your_text_domain' );
    $new['telephone'] = __( 'Telephone', 'your_text_domain' );
    $new['date'] = __( 'Date', 'your_text_domain' );
    return $new;
}

// Add the data to the custom columns for the client post type:
add_action( 'manage_client_posts_custom_column' , 'custom_client_column', 10, 2 );
function custom_client_column( $column, $post_id ) {
    switch ( $column ) {
        case 'email' :
            echo get_post_meta( $post_id , 'wcl_email' , true ); 
            break;
        case 'telephone' :
            echo get_post_meta( $post_id , 'wcl_telephone' , true ); 
            break;
    }
}

function add_shortcode_to_add_front_end( $atts ){
    return "
        <div id='wcl-step-1'>
            <p>Agrega tus datos y recibe ¡una promoción especial!</p>
            <form id='client-add'>
                <label>Nombre</label>
                <input type='text' name='name' placeholder='Nombre' required />
                <label>Email</label>
                <input type='text' name='email' placeholder='Email' required />
                <label>Teléfono</label>
                <input type='text' name='telephone' placeholder='Teléfono' required />
                <input type='submit' value='Enviar' />
            </form>
        </div>
        <div id='wcl-step-2' style='display:none;'>
            <h3>¡Felicidades!</h3>
            <p>Ahora tienes tu cupón</p>
            <p class='cupon'>15especial</p>
            <p>Canjealo en tu carrito de compras antes de completar el pago.</p>
        </div>
    ";
}
add_shortcode( 'offer_for_coupon', 'add_shortcode_to_add_front_end' );

// Activate shortcodes in widget text
add_filter('widget_text','do_shortcode');


function add_client_script() {
    wp_enqueue_script( 'client-script', plugin_dir_url( __FILE__ ) . '/script.js', array ( 'jquery' ), 1.1, true);
    wp_localize_script( 'client-script', 'my_ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'add_client_script' );

add_action( 'wp_ajax_add_client', 'ajax_add_client' );
add_action( 'wp_ajax_nopriv_add_client', 'ajax_add_client' );

function ajax_add_client() {
    $post_id = wp_insert_post(array (
        'post_type' => 'client',
        'post_title' => $_POST['name'],
        'post_status' => 'publish',
        'comment_status' => 'closed',   // if you prefer
        'ping_status' => 'closed',      // if you prefer
    ));
    if ($post_id) {
        // insert post meta
        add_post_meta($post_id, 'wcl_email', $_POST['email']);
        add_post_meta($post_id, 'wcl_telephone', $_POST['telephone']);
        echo 'true';
    } else {
        echo 'false';
    }
    wp_die();
}

?>
