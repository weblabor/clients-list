jQuery('#client-add').submit(function(event) {
    event.preventDefault();
    var data = jQuery("#client-add").serialize() + "&action=add_client";
    jQuery.post(
        my_ajax_object.ajax_url, 
        data, 
        function(response){
            if(response=='true') {
                jQuery("#wcl-step-1").css('display', 'none');
                jQuery("#wcl-step-2").css('display', 'block');
            }
        }
    );
});