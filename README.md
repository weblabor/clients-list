# Clients Wordpress Plugin

**Description:** Plugin to show and add clients. 

Code of example
--------------
```php
$r = new WP_Query( array(
   'posts_per_page' => 5,
   'no_found_rows' => true, /*suppress found row count*/
   'post_status' => 'publish',
   'post_type' => 'client',
   'ignore_sticky_posts' => true,
) );
if ($r->have_posts()) :
while ( $r->have_posts() ) : $r->the_post(); 
	$link = get_post_meta(null, 'wsl_link', true);
	if($link != '') {
        echo '<a href="'.$link.'">';
    } 
    echo get_the_post_thumbnail(null, 'full', array('class' => 'mySlides', 'style' => 'width:100%;')); 
    if($link != '') {
        echo '</a>';
    } 
endwhile;
// Reset the global $the_post as this query will have stomped on it
wp_reset_postdata();
endif;
```