<?php

require_once plugin_dir_path( __FILE__ ) . 'class-wcl-custom-type.php';
require_once plugin_dir_path( __FILE__ ) . 'metaboxes/wcl_metaboxes.php';
require_once plugin_dir_path( __FILE__ ) . 'metaboxes/metabox-telephone.php';
require_once plugin_dir_path( __FILE__ ) . 'metaboxes/metabox-email.php';
