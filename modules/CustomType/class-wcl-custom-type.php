<?php

/**
 * This class defines all functionality for the dashboard
 * of the plugin
 *
 * @package WCLM
 */

class WCL_Custom_Type {
    
    public function create_post_type() {
        register_post_type( 'client',
            array(
                'labels' => array(
                    'name' => __( 'Clients' ),
                    'singular_name' => __( 'Client' )
                ),
                'public' => true,
                'has_archive' => false,
                'supports'=> array("title","author")
            )
        );
    }

}
