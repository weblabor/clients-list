<?php

/**
 * Class for making the metabox called Year
 *
 * @package WCLM
 */

class Metabox_Telephone implements wcl_metaboxes {
    
    private $id = "telephone";
    private $title = "Telefono";
    private $screen = "client";
    private $context = "normal";
    private $priority = "default";

    public function __construct($loader) {
    	$loader->add_action("admin_init", $this, "create");
    	$loader->add_action('save_post', $this, 'save');
    }

    public function create() {
    	add_meta_box( $this->id, $this->title, array($this, 'display'), $this->screen, $this->context, $this->priority);
    }

    public function display() {
    	global $post;
	  	$field = get_post_meta($post->ID, 'wcl_telephone', true);
	  	wp_nonce_field( 'wcl_telephone_meta_box_nonce', 'wcl_telephone_meta_box_nonce' );

	  	?>
	 		<input type="text" class="widefat" name="telephone" value="<?php if($field != '') echo esc_attr( $field ); ?>" />
	   
	  	<?php
    }

    public function save($post_id) {
    	if ( ! isset( $_POST['wcl_telephone_meta_box_nonce'] ) ||
	  	! wp_verify_nonce( $_POST['wcl_telephone_meta_box_nonce'], 'wcl_telephone_meta_box_nonce' ) )
	    	return;
	  
	  	if (!current_user_can('edit_post', $post_id))
	    	return;
	  
	  	$old = get_post_meta($post_id, 'wcl_telephone', true);
	  	$new = $_POST['telephone'];

		if ( !empty( $new ) && $new != $old )
			update_post_meta( $post_id, 'wcl_telephone', $new );
		elseif ( empty($new) && $old )
		    delete_post_meta( $post_id, 'wcl_telephone', $old );
    }

}
