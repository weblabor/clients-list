<?php

/**
 * Interface for metaboxes
 *
 * @package WCLM
 */

interface wcl_metaboxes {

    public function __construct($loader);
    public function create();
    public function display();
    public function save($post_id);

}
